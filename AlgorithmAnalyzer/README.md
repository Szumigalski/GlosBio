### Jak wszystko odpalić?  
Potrzebne są:  
* Python 3
* pipenv
* npm
*  [ffmpeg](http://ffmpeg.org) - dodany do PATH na systemie

#### API
wchodzimy w folder `Backend`
* odpalamy `python run.py` lub `pipenv install && pipenv run python main.py`
* cieszymy się postawionym serwerem na porcie 5000
* pliki .wav zapisywane są w folderze Backend/data
 
#### Aplikacja webowa
* wchodzimy w folder `frontend`
* odpalamy `npm install && npm run start`
* okno z apką wyskoczy samo w domyślnej przeglądarce
