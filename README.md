# GlosBio
Projekt inżynierskie Głos Biometryczny

## Aby uruchomić aplikację webową należy:
 - wejść do katalogu AlgorithmAnalyzer/frontend
 - użyć w konsoli polecenia **npm install** (należy użyć tego tylko raz do stworzenia katalogu *node modules*)
 - jeśli chcemy uruchomić aplikację należy użyć polecenia **npm start**
 - aplikacja znajduje się na **localhost:3000**
